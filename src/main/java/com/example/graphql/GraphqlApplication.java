package com.example.graphql;

import com.example.graphql.service.InsertMovieDataService;
import com.example.graphql.service.RolePermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GraphqlApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(GraphqlApplication.class, args);
	}

	@Autowired
	InsertMovieDataService insertMovieDataService;
	@Autowired
	RolePermissionService rolePermissionService;

	@Override
	public void run(String... args) throws Exception {
//        insertMovieDataService.insertMovie();
//		insertMovieDataService.invertedIndexList();
		rolePermissionService.insertRole();
		rolePermissionService.insertPermission();
	}

}
