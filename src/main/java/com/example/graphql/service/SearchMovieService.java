package com.example.graphql.service;

import com.example.graphql.entity.Movie;
import org.springframework.stereotype.Service;

import java.util.List;

public interface SearchMovieService {
    public List<Movie> allMovie();
    public List<Movie> fullSearchMovie(String wordSearch);
    public List<Movie> partialSearchMovie(String wordSearch);
    public List<Movie> invertedIndexSearchMovie(String wordSearch);
}
