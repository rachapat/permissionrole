package com.example.graphql.service.implement;

import com.example.graphql.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;

import java.util.ArrayList;
import java.util.List;

public class CommonImp {

    @Autowired
    ResourceLoader loader;

    @Autowired
    MovieRepository movieRepo;

    @Autowired
    CastRepository castRepo;

    @Autowired
    GenresRepository genresRepo;

    @Autowired
    RoleRepository roleRepo;

    @Autowired
    PermissionRepository permissionRepo;


    public static <T> List<T> removeDuplicates(List<T> list)
    {

        // Create a new ArrayList
        List<T> newList = new ArrayList<T>();

        // Traverse through the first list
        for (T element : list) {

            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {

                newList.add(element);
            }
        }

        // return the new list
        return newList;
    }

    public <T> List<T> intersection(List<T> list1, List<T> list2) {
        List<T> list = new ArrayList<T>();

        for (T t : list1) {
            if(list2.contains(t)) {
                list.add(t);
            }
        }

        return list;
    }

}
