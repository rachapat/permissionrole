package com.example.graphql.service.implement;

import com.example.graphql.entity.Permission;
import com.example.graphql.entity.Role;
import com.example.graphql.entity.Status.PermissionStatus;
import com.example.graphql.entity.Status.RoleStatus;
import com.example.graphql.service.RolePermissionService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RolePermissionImp extends CommonImp implements RolePermissionService {

    public void insertRole(){
        for(RoleStatus rs: RoleStatus.values()){
            if(!roleRepo.existsByRoleLike(rs.getRole())){
                Role role = new Role();
                role.setRole(rs.getRole());
                System.out.println(rs.getRole());
                roleRepo.saveAndFlush(role);
            }
        }
    }

    public void insertPermission(){
        for(PermissionStatus ps: PermissionStatus.values()){
            if(!permissionRepo.existsByPermissionLike(ps.getPernission())){
                Permission permission = new Permission();
                permission.setPermission(ps.getPernission());
                System.out.println(ps.getPernission());
                permissionRepo.saveAndFlush(permission);
            }
        }
    }
}
