package com.example.graphql.service.implement;

import com.example.graphql.service.InsertMovieDataService;
import com.example.graphql.cacde.Cacde;
import com.example.graphql.dto.MovieDataDTO;
import com.example.graphql.entity.Cast;
import com.example.graphql.entity.Genres;
import com.example.graphql.entity.Movie;
import com.example.graphql.service.InsertMovieDataService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class InsertMovieDataImp extends CommonImp implements InsertMovieDataService {

    public void insertMovie() throws IOException {
        String url = "https://raw.githubusercontent.com/prust/wikipedia-movie-data/master/movies.json";
        Resource resource = loader.getResource(url);
        InputStream inputStream = resource.getInputStream();
        ObjectMapper mapper = new ObjectMapper();
        byte[] bdata = FileCopyUtils.copyToByteArray(inputStream);
        String json = new String(bdata, StandardCharsets.UTF_8);
//        System.out.println(json);


        List<MovieDataDTO> movieList = mapper.readValue(json, new TypeReference<List<MovieDataDTO>>() {});
        for(MovieDataDTO data : movieList){

            if(!movieRepo.existsByMovieNameLikeAndYear(data.getTitle(),Long.parseLong(data.getYear()))){
                Movie movie = new Movie();
                List<Cast> casts = new ArrayList<>();
                List<Genres> genres = new ArrayList<>();

                movie.setMovieName(data.getTitle());
                movie.setYear(Long.parseLong(data.getYear()));

                movieRepo.saveAndFlush(movie);

                if(data.getCast().size()>0){
                    for(String s : data.getCast()){
                        if(!castRepo.existsByCastNameLike(s)){
                            Cast cast = new Cast();
                            cast.setCastName(s);
                            castRepo.saveAndFlush(cast);
                        }
                        casts.add(castRepo.findByCastNameLike(s));
                    }
                    movie.setCastsList(casts);
                }

                if(data.getGenres().size()>0){
                    for(String s : data.getGenres()){
                        if(!genresRepo.existsByGenresNameLike(s)){
                            Genres genres1 = new Genres();
                            genres1.setGenresName(s);
                            genresRepo.saveAndFlush(genres1);
                        }
                        genres.add(genresRepo.findByGenresNameLike(s));
                    }
                    movie.setGenresList(genres);
                }

                movieRepo.saveAndFlush(movie);
            }

        }
    }

    public void invertedIndexList(){
        List<Movie> movies = movieRepo.findAll();
        if(movies.size()>0){
            for(Movie m:movies){
                List<String> wordList = removeDuplicates(
                        Arrays.stream(m.getMovieName().split(" "))
                                .map(String::trim).collect(Collectors.toList()));
//                System.out.println(Arrays.toString(wordList.toArray()));
                for(String s: wordList){
                    if(!existsByWordLikeAndIdMovieListIn(s,m.getId())){
                        if(!existsByWordLike(s)){
                            saveNewInvertedIndex(s,m.getId());

                        }else{
                            saveInvertedIndex(s, m.getId());

                        }
                    }
                }

            }
        }
    }

    public boolean existsByWordLikeAndIdMovieListIn(String word,Long idmovielist){

        if(Cacde.invertedIndexList.get(word.toLowerCase(Locale.ROOT))!=null){
            return Cacde.invertedIndexList.get(word.toLowerCase(Locale.ROOT)).contains(idmovielist);
        }
        return false;
    }

    public boolean existsByWordLike(String word){

        if(Cacde.invertedIndexList.get(word.toLowerCase(Locale.ROOT))!=null){
            return true;
        }
        return false;
    }

    public List<Long> findByWordLike(String word){
        return Cacde.invertedIndexList.get(word.toLowerCase(Locale.ROOT));
    }

    public void saveInvertedIndex(String word,Long id){
        List<Long> idMovieList = Cacde.invertedIndexList.get(word.toLowerCase(Locale.ROOT));
        idMovieList.add(id);
        Cacde.invertedIndexList.put(word.toLowerCase(Locale.ROOT),idMovieList);
    }

    public void saveNewInvertedIndex(String word,Long idlist){
        List<Long> idMovieList = new ArrayList<>();
        idMovieList.add(idlist);
        Cacde.invertedIndexList.put(word.toLowerCase(Locale.ROOT),idMovieList);
    }

}
