package com.example.graphql.service;

public interface RolePermissionService {
    public void insertRole();
    public void insertPermission();
}
