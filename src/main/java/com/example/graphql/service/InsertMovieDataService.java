package com.example.graphql.service;

import java.io.IOException;

public interface InsertMovieDataService {
    public void insertMovie() throws IOException;
    public void invertedIndexList();
}
