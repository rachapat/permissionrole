//package com.example.graphql.entity;
//
//import javax.persistence.*;
//
//@Entity
//@Table(name = "User_Role")
//public class UserRole {
//    @Id
//    @GeneratedValue(strategy= GenerationType.IDENTITY)
//    private Long id;
//    @ManyToOne
//    @JoinColumn(name = "RoleId")
//    private Role role = new Role();
//    @ManyToOne
//    @JoinColumn(name = "UserId")
//    private User user = new User();
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public Role getRole() {
//        return role;
//    }
//
//    public void setRole(Role role) {
//        this.role = role;
//    }
//
//    public User getUser() {
//        return user;
//    }
//
//    public void setUser(User user) {
//        this.user = user;
//    }
//}
