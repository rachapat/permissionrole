package com.example.graphql.entity.Status;

public enum PermissionStatus {
    VIEW_MOVIE_ID("view movie id"),
    VIEW_MOVIE_NAME("view movie name"),
    VIEW_MOVIE_YAER("view movie year"),
    VIEW_MOVIE_CASTS("view movie casts"),
    VIEW_MOVIE_GENRES("view movie genres");

    String permission;
    PermissionStatus(String permission) {
        this.permission = permission;
    }
    public String getPernission() {
        return this.permission;
    }
}
