package com.example.graphql.entity.Status;

public enum RoleStatus {
    ADMIN("admin"),
    OPERATOR("operator"),
    VIEWER("viewer");

    String role;

    RoleStatus(String role) {
        this.role = role;
    }

    public String getRole() {
        return this.role;
    }
}
