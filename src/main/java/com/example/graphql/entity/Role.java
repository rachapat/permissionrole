package com.example.graphql.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Role {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String role;
    @OneToMany(mappedBy="role")
    @JsonIgnore
    private List<RolePermission> RolePermissionList = new ArrayList<>();
    @JsonIgnore
    @OneToMany(mappedBy="role")
    private List<User> userList = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<RolePermission> getRolePermissionList() {
        return RolePermissionList;
    }

    public void setRolePermissionList(List<RolePermission> rolePermissionList) {
        RolePermissionList = rolePermissionList;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
}
