package com.example.graphql.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
//@Table(name = "movie")
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String movieName;
    private Long year;
    @ManyToMany(cascade = {CascadeType.PERSIST,
            CascadeType.DETACH,
            CascadeType.REFRESH,
            CascadeType.REMOVE},fetch = FetchType.LAZY)
    @JoinTable(name = "Movie_Cast",
            joinColumns = @JoinColumn(name ="movieId", referencedColumnName = "id"),
            inverseJoinColumns  = @JoinColumn(name = "castId", referencedColumnName = "id")
    )
    private List<Cast> castsList =new ArrayList<>();
    @ManyToMany(cascade = {CascadeType.PERSIST,
            CascadeType.DETACH,
            CascadeType.REFRESH,
            CascadeType.REMOVE},fetch = FetchType.LAZY)
    @JoinTable(name = "Movie_Genres",
            joinColumns = @JoinColumn(name ="movieId", referencedColumnName = "id"),
            inverseJoinColumns  = @JoinColumn(name = "genresId", referencedColumnName = "id")
    )
    private List<Genres> genresList =new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public List<Cast> getCastsList() {
        return castsList;
    }

    public void setCastsList(List<Cast> castsList) {
        this.castsList = castsList;
    }

    public List<Genres> getGenresList() {
        return genresList;
    }

    public void setGenresList(List<Genres> genresList) {
        this.genresList = genresList;
    }

}
