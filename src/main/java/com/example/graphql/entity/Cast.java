package com.example.graphql.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Cast implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String castName;

    @JsonIgnore
    @ManyToMany(mappedBy = "castsList",cascade = CascadeType.ALL)
//    @JoinTable(name = "Movie_Cast",
//            joinColumns = @JoinColumn(name ="castId", referencedColumnName = "castId"),
//            inverseJoinColumns  = @JoinColumn(name = "movieId", referencedColumnName = "movieId")
//    )
    private List<Movie> movieList = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public String getCastName() {
        return castName;
    }

    public void setCastName(String castName) {
        this.castName = castName;
    }

    public List<Movie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }
}
