package com.example.graphql.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Genres implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String genresName;

    @JsonIgnore
    @ManyToMany(mappedBy = "genresList",cascade = CascadeType.ALL)
//    @JoinTable(name = "Movie_Genres",
//            joinColumns = @JoinColumn(name ="genresId", referencedColumnName = "genresId"),
//            inverseJoinColumns  = @JoinColumn(name = "movieId", referencedColumnName = "movieId")
//    )
    private List<Movie> movieList = new ArrayList<>();


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGenresName() {
        return genresName;
    }

    public void setGenresName(String genresName) {
        this.genresName = genresName;
    }

    public List<Movie> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<Movie> movieList) {
        this.movieList = movieList;
    }
}
