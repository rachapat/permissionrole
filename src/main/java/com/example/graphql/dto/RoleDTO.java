package com.example.graphql.dto;

public class RoleDTO {
    public String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
