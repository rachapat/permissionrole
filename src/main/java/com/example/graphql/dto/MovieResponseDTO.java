package com.example.graphql.dto;

import com.example.graphql.entity.Cast;
import com.example.graphql.entity.Genres;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class MovieResponseDTO implements Serializable {

    private Long id;
    private String movieName;
    private Long year;
    private List<String> castsList =new ArrayList<>();
    private List<String> genresList =new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public List<String> getCastsList() {
        return castsList;
    }

    public void setCastsList(List<String> castsList) {
        this.castsList = castsList;
    }

    public List<String> getGenresList() {
        return genresList;
    }

    public void setGenresList(List<String> genresList) {
        this.genresList = genresList;
    }
}
