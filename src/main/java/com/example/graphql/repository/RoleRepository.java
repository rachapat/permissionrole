package com.example.graphql.repository;

import com.example.graphql.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,Long> {
    boolean existsByRoleLike(String role);
    Role findByRoleLike(String role);
}
