package com.example.graphql.repository;

import com.example.graphql.entity.Role;
import com.example.graphql.entity.RolePermission;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RolePermissionRepository extends JpaRepository<RolePermission,Long> {
    List<RolePermission> findByRole_Role(String role);
    List<RolePermission> findByRole(Role role);
}
