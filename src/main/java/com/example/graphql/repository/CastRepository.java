package com.example.graphql.repository;

import com.example.graphql.entity.Cast;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CastRepository extends JpaRepository<Cast, Long> {
    boolean existsByCastNameLike(String Cast);
    Cast findByCastNameLike(String Cast);
}
