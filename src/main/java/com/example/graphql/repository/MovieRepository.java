package com.example.graphql.repository;

import com.example.graphql.entity.Cast;
import com.example.graphql.entity.Movie;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.HashSet;
import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long> {
    boolean existsByMovieNameLike(String moviename);
    boolean existsByMovieNameLikeAndYear(String moviename,Long year);
    Movie findMovieById(Long id);
    Movie findByMovieName(String moviename);
    List<Movie> findAllByMovieNameLike(String name);

    boolean existsByMovieNameIgnoreCaseContaining(String name);
    List<Movie> findAllByMovieNameIgnoreCaseContaining(String name);
    List<Movie> findAllByIdIn(HashSet<Long> idList);
}
