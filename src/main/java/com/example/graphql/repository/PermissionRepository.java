package com.example.graphql.repository;

import com.example.graphql.entity.Permission;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PermissionRepository extends JpaRepository<Permission,Long> {
    boolean existsByPermissionLike(String permission);

}
