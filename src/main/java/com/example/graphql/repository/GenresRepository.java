package com.example.graphql.repository;

import com.example.graphql.entity.Cast;
import com.example.graphql.entity.Genres;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GenresRepository extends JpaRepository<Genres, Long> {
    boolean existsByGenresNameLike(String genres);
    Genres findByGenresNameLike(String genres);
}
