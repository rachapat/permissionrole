package com.example.graphql.controller;

import com.example.graphql.entity.Movie;
import com.example.graphql.entity.Status.RoleStatus;
import com.example.graphql.entity.User;
import com.example.graphql.repository.UserRepository;
import com.example.graphql.service.SearchMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@Controller
@RestController
public class MoiveController {

    @Autowired
    SearchMovieService searchMovieService;

    @Autowired
    UserRepository userRepo;

    @PostMapping(value = "/")
    public String test() throws IOException {
        System.out.println(RoleStatus.ADMIN.getRole());
        return RoleStatus.ADMIN.getRole();
    }

    @PostMapping(value = "/movie/full-search/{wordSearch}")
    public List<Movie> fullSearchMovie(@PathVariable String wordSearch) throws IOException {
        List<Movie> movieList = searchMovieService.fullSearchMovie(wordSearch);
        return movieList;
    }

    @PostMapping(value = "/movie/partial-search/{wordSearch}")
    public List<Movie> partialSearchMovie(@PathVariable String wordSearch) throws IOException {
        List<Movie> movieList = searchMovieService.partialSearchMovie(wordSearch);
        return movieList;
    }

    @PostMapping(value = "/movie/index-search/{wordSearch}")
    public List<Movie> wordSearch(@PathVariable String wordSearch) throws IOException {
        List<Movie> movieList = searchMovieService.invertedIndexSearchMovie(wordSearch);
        return movieList;
    }

    @PostMapping(value = "/user/{wordSearch}")
    public User user(@PathVariable String wordSearch) throws IOException {
        return userRepo.findByEmail(wordSearch);
    }


}
