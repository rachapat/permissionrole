package com.example.graphql.resolver;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.example.graphql.dto.MovieResponseDTO;
import com.example.graphql.entity.*;
import com.example.graphql.entity.Status.PermissionStatus;
import com.example.graphql.repository.*;
import com.example.graphql.service.SearchMovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SelectMovieImp implements GraphQLQueryResolver, GraphQLMutationResolver {

    @Autowired
    SearchMovieService searchMovieService;

    @Autowired
    MovieRepository movieRepo;

    @Autowired
    CastRepository castRepo;

    @Autowired
    GenresRepository genresRepo;

    @Autowired
    RoleRepository roleRepo;

    @Autowired
    PermissionRepository permissionRepo;

    @Autowired
    RolePermissionRepository rolePermissionRepo;

    @Autowired
    UserRepository userRepo;

    public List<Movie> allMovie(String role){
        if(roleRepo.existsByRoleLike(role)){
            Role rolethis = roleRepo.findByRoleLike(role);
            List<RolePermission> rolePermissionList = rolePermissionRepo.findByRole(rolethis);
            if(rolePermissionList.size()>0){
                List<Movie> movieList = searchMovieService.allMovie();
//                List<MovieResponseDTO> rep = new ArrayList<>();
                List<Movie> rep = new ArrayList<>();
                if(movieList.size()>0){
                    for(Movie movie:movieList){
                        Movie movie1 = new Movie();
                        for(RolePermission rolePermission : rolePermissionList){
                            if(PermissionStatus.VIEW_MOVIE_ID.getPernission().equalsIgnoreCase(rolePermission.getPermission().getPermission())){
                                movie1.setId(movie.getId());
                                System.out.println(movie.getId());
                                continue;
                            }
                            if(PermissionStatus.VIEW_MOVIE_NAME.getPernission().equalsIgnoreCase(rolePermission.getPermission().getPermission())){
                                movie1.setMovieName(movie.getMovieName());
                                System.out.println(movie.getMovieName());
                                continue;
                            }
                            if(PermissionStatus.VIEW_MOVIE_YAER.getPernission().equalsIgnoreCase(rolePermission.getPermission().getPermission())){
                                movie1.setYear(movie.getYear());
                                System.out.println(movie.getYear());
                                continue;
                            }
                            if(PermissionStatus.VIEW_MOVIE_CASTS.getPernission().equalsIgnoreCase(rolePermission.getPermission().getPermission())){
                                List<Cast> casts = new ArrayList<>(movie.getCastsList());
                                System.out.println(casts.size());
                                movie1.setCastsList(casts);
                                continue;
                            }
                            if(PermissionStatus.VIEW_MOVIE_GENRES.getPernission().equalsIgnoreCase(rolePermission.getPermission().getPermission())){
                                List<Genres> genres = new ArrayList<>(movie.getGenresList());
                                System.out.println(genres.size());
                                movie1.setGenresList(genres);
                            }
                        }
                        rep.add(movie1);
                    }
                    return rep;
                }

            }

        }
        return null;
    }

    public List<Movie> fullSearchMovie(String role,String movieName){

        if(roleRepo.existsByRoleLike(role)){
            Role rolethis = roleRepo.findByRoleLike(role);
            List<RolePermission> rolePermissionList = rolePermissionRepo.findByRole(rolethis);
            if(rolePermissionList.size()>0){
                List<Movie> movieList = searchMovieService.fullSearchMovie(movieName);
//                List<MovieResponseDTO> rep = new ArrayList<>();
                List<Movie> rep = new ArrayList<>();
                if(movieList.size()>0){
                    for(Movie movie:movieList){
                        Movie movie1 = new Movie();
                        for(RolePermission rolePermission : rolePermissionList){
                            if(PermissionStatus.VIEW_MOVIE_ID.getPernission().equalsIgnoreCase(rolePermission.getPermission().getPermission())){
                                movie1.setId(movie.getId());
                                System.out.println(movie.getId());
                                continue;
                            }
                            if(PermissionStatus.VIEW_MOVIE_NAME.getPernission().equalsIgnoreCase(rolePermission.getPermission().getPermission())){
                                movie1.setMovieName(movie.getMovieName());
                                System.out.println(movie.getMovieName());
                                continue;
                            }
                            if(PermissionStatus.VIEW_MOVIE_YAER.getPernission().equalsIgnoreCase(rolePermission.getPermission().getPermission())){
                                movie1.setYear(movie.getYear());
                                System.out.println(movie.getYear());
                                continue;
                            }
                            if(PermissionStatus.VIEW_MOVIE_CASTS.getPernission().equalsIgnoreCase(rolePermission.getPermission().getPermission())){
                                List<Cast> casts = new ArrayList<>(movie.getCastsList());
                                System.out.println(casts.size());
                                movie1.setCastsList(casts);
                                continue;
                            }
                            if(PermissionStatus.VIEW_MOVIE_GENRES.getPernission().equalsIgnoreCase(rolePermission.getPermission().getPermission())){
                                List<Genres> genres = new ArrayList<>(movie.getGenresList());
                                System.out.println(genres.size());
                                movie1.setGenresList(genres);
                            }
                        }
                        rep.add(movie1);
                    }
                    return rep;
                }

            }

        }
        return null;
    }

//    public List<Movie> partialSearchMovie(String movieName){
//        List<Movie> movieList = searchMovieService.partialSearchMovie(movieName);
//        return movieList;
//    }
//
//    public List<Movie> invertedIndexSearchMovie(String movieName){
//        List<Movie> movieList = searchMovieService.invertedIndexSearchMovie(movieName);
//        return movieList;
//    }

    public User user(String email){
        return userRepo.findByEmail(email);
    }
}
